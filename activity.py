# 1. Create an abstract class called Animal that has the following abstract
# methods:
# a. eat(food)
# b. make_sound()

# Parent class

from abc import ABC, abstractclassmethod
class Animal():
    @abstractclassmethod
    def make_sound(self):
        pass
    def eat(self):
        pass
    def call(self):
        pass

# 2. Create two classes that implements the Animal class called Cat and Dog
# with each of the following properties and methods:
# a. Properties:
# i. Name
# ii. Breed
# iii. Age
# b. Methods:
# i. Getters
# ii. Setters
# iii. Implementation of abstract methods
# iv. call()   

# Child classes
class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age
        
    def eat(self,food):
        print(f"Eaten {food}")
    def make_sound(self):
        print("Bark! Woof! Arf!")
    def call(self):
        print(f"Here {self._name}")
        
class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age
    def eat(self,food):
        print(f"Eaten {food}")
    def make_sound(self):
        print("Meow! Nyaw! Nyaaaa!")
    def call(self):
        print(f"{self._name}, come on!")

# Test Cases:
dog = Dog("Hatchi", "Shnauzer", 6)
dog.eat("Steak")
dog.make_sound()
dog.call()

cat = Cat("Cardo", "PusaKal", 10)
cat.eat("Fish")
cat.make_sound()
cat.call()